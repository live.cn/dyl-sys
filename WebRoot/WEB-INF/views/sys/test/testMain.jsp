<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{todo}</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote" action="test!main.do" id="mainForm" method="post">
				<div class="layui-fr">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<dyl:hasPermission kind="ADD" menuId="${menuId}">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
					</dyl:hasPermission>
					<dyl:hasPermission kind="DELETE" menuId="${menuId}">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="delete">
						<i class="layui-icon">&#xe640;</i> 删除
					</a>
					</dyl:hasPermission>
				</div>
    			<div class="layui-input-inline">
			        <label class="layui-form-label">测试1:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="test1"  value="${test.test1}" placeholder="请输入测试1" class="layui-input">
			        </div>
			    </div>
    			<div class="layui-input-inline">
			        <label class="layui-form-label">测试2:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="yrdy2"  value="${test.yrdy2}" placeholder="请输入测试2" class="layui-input">
			        </div>
			    </div>
    			<div class="layui-input-inline">
			        <label class="layui-form-label">测试3:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="test3"  value="${test.test3}" placeholder="请输入测试3" class="layui-input">
			        </div>
			    </div>
				
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th class="table-check"><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
								<th>测试1</th>
								<th>测试2</th>
								<th>测试3</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${testList}" var="o">
								<tr>
							        <td><input type="checkbox" lay-skin="primary" data-opt="check" data-id="${o.id}"></td>
									<td>${o.test1}</td>
									<td>${o.yrdy2}</td>
									<td>${o.test3}</td>
							        <td>
										<dyl:hasPermission kind="UPDATE" menuId="${menuId}">
										<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="${o.id}" data-opt="edit">
											<i class="layui-icon">&#xe642;</i> 修改
										</a>
										</dyl:hasPermission>
									</td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<!-- 分页 -->
		<%@include file="/WEB-INF/views/common/page.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				//将分页的 的数据带过来
				$('#mainForm').append($('#pageForm').html());
				showLoading();//显示等待框
				$('#mainForm').submit();
			});
			//添加方法
			$('#add').click(function(){
				var para={
					url:"test!testForm.do",
					title:"添加",
					btnOK:"保存"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//修改方法
			$('[data-opt=edit]').click(function(){
				var para={
					url:"test!testForm.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改",
					btnOK:"修改"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//删除方法
			$('#delete').click(function(){
				var dataIds = getdataIds("check");
				if(dataIds){
					layer.confirm('确认删除所选择的吗?', function(index){
						getJsonDataByPost("test!delete.do","dataIds="+dataIds,function(data){
							if(data.result){
								layer.alert("删除成功!",function(){
									$('#search').click();//查询
								}); 
							}else{
								l.alert(data.msg);//错误消息弹出
							}
							layer.close(index);//关闭删除确认提示框
						},true); 
					});
				}
			});
		</script>
	</body>
</html>