<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>代码生成器</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote" action="easyCode!main.do" id="mainForm" method="post">
				<div class="layui-fr">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
				</div>
    			<div class="layui-input-inline">
			        <label class="layui-form-label">表/视图名:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="name"  value="${easyCode.name}" placeholder="请输入" class="layui-input">
			        </div>
			    </div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th>表/视图名</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${easyCodeList}" var="o">
								<tr>
									<td>${o.name}</td>
							        <td>
										<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="${o.name}" data-opt="scdm">
											<i class="layui-icon">&#xe642;</i> 生成代码
										</a>
									</td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<!-- 分页 -->
		<%@include file="/WEB-INF/views/common/page.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				//将分页的 的数据带过来
				$('#mainForm').append($('#pageForm').html());
				showLoading();//显示等待框
				$('#mainForm').submit();
			});
			//生成代码
			$('[data-opt="scdm"]').click(function(){
				var para={
					url:"easyCode!easyCodeForm.do",
					para:"name="+$(this).attr("data-id"),
					title:"代码生成器",
					btnOK:"生成",
					area: ['500px', '600px']
				};
				addOrUpdate(para);
			});
		</script>
	</body>
</html>