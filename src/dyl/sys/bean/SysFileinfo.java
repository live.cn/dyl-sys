package dyl.sys.bean;

/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-06-06 15:31:42
*/

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;
import java.util.Date;

public class SysFileinfo implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:是
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：文件存储的真实名称
	*对应db字段名:real_name 类型:VARCHAR2(100)  
	*是否可以为空:是
	*/
	
	private  String  realName;
	
	/*字段说明：上传文件名称
	*对应db字段名:upload_name 类型:VARCHAR2(100)  
	*是否可以为空:是
	*/
	
	private  String  uploadName;
	
	/*字段说明：文件大小
	*对应db字段名:file_size 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  fileSize;
	
	/*字段说明：文件下载地址
	*对应db字段名:file_url 类型:VARCHAR2(200)  
	*是否可以为空:是
	*/
	
	private  String  fileUrl;
	
	/*字段说明：上传人
	*对应db字段名:creator 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  creator;
	
	/*字段说明：上传时间
	*对应db字段名:create_time 类型:DATE(7)  
	*是否可以为空:是
	*/
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private  Date  createTime;
	
	/*字段说明：关联的业务id
	*对应db字段名:c_id 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  cId;
	
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }
    public String getUploadName() {
        return uploadName;
    }
    public void setUploadName(String uploadName) {
        this.uploadName = uploadName;
    }
    public BigDecimal getFileSize() {
        return fileSize;
    }
    public void setFileSize(BigDecimal fileSize) {
        this.fileSize = fileSize;
    }
    public String getFileUrl() {
        return fileUrl;
    }
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
    public BigDecimal getCreator() {
        return creator;
    }
    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public BigDecimal getCId() {
        return cId;
    }
    public void setCId(BigDecimal cId) {
        this.cId = cId;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"realName:"+realName+"\n"+
		
		"uploadName:"+uploadName+"\n"+
		
		"fileSize:"+fileSize+"\n"+
		
		"fileUrl:"+fileUrl+"\n"+
		
		"creator:"+creator+"\n"+
		
		"createTime:"+createTime+"\n"+
		
		"cId:"+cId+"\n"+
		"";
	}
}
